/**
 * App user list
 */

$(function () {
    "use strict";

    var dataTablePermissions = $(".datatables-permissions"),
        assetPath = "../../../app-assets/",
        dt_permission,
        userList = "app-user-list.html";

    if ($("body").attr("data-framework") === "laravel") {
        assetPath = $("body").attr("data-asset-path");
        userList = assetPath + "app/user/list";
    }

    // Users List datatable
    if (dataTablePermissions.length) {
        dt_permission = dataTablePermissions.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "/admin/datatables/permission/all",
                type: "GET",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    ),
                    "Content-Type": "application/json",
                    Accept: "application/json",
                },
                contentType: "application/json",
                dataType: "json",
            },
            columns: [
                // columns according to JSON
                { data: "name" },
                { data: "action" },
            ],
            columnDefs: [
                {
                    // Actions
                    targets: -1,
                    title: "Actions",
                    orderable: false,
                    render: function (data, type, full, meta) {
                        var $id = full["id"];
                        return (
                            '<a href="javascript:;" class="btn btn-sm btn-icon edit-record" data-item="' +
                            $id +
                            '">' +
                            feather.icons["edit"].toSvg({
                                class: "font-medium-2 text-body",
                            }) +
                            "</i></a>" +
                            '<a href="javascript:;" class="btn btn-sm btn-icon delete-record"  data-item="' +
                            $id +
                            '">' +
                            feather.icons["trash"].toSvg({
                                class: "font-medium-2 text-body",
                            }) +
                            "</a>"
                        );
                    },
                },
            ],
            order: [[1, "asc"]],
            dom:
                '<"d-flex justify-content-between align-items-center header-actions text-nowrap mx-1 row mt-75"' +
                '<"col-sm-12 col-lg-4 d-flex justify-content-center justify-content-lg-start" l>' +
                '<"col-sm-12 col-lg-8"<"dt-action-buttons d-flex align-items-center justify-content-lg-end justify-content-center flex-md-nowrap flex-wrap"<"me-1"f><"user_role mt-50 width-200 me-1">B>>' +
                '><"text-nowrap" t>' +
                '<"d-flex justify-content-between mx-2 row mb-1"' +
                '<"col-sm-12 col-md-6"i>' +
                '<"col-sm-12 col-md-6"p>' +
                ">",
            language: {
                sLengthMenu: "Show _MENU_",
                search: "Search",
                searchPlaceholder: "Search..",
            },
            // Buttons with Dropdown
            buttons: [
                {
                    text: "Add Permission",
                    className: "add-new btn btn-primary mt-50",
                    attr: {
                        "data-bs-toggle": "modal",
                        "data-bs-target": "#addPermissionModal",
                    },
                    init: function (api, node, config) {
                        $(node).removeClass("btn-secondary");
                    },
                },
            ],
            // For responsive popup
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return "Details of Permission";
                        },
                    }),
                    type: "column",
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                      col.rowIndex +
                                      '" data-dt-column="' +
                                      col.columnIndex +
                                      '">' +
                                      "<td>" +
                                      col.title +
                                      ":" +
                                      "</td> " +
                                      "<td>" +
                                      col.data +
                                      "</td>" +
                                      "</tr>"
                                : "";
                        }).join("");

                        return data
                            ? $('<table class="table"/><tbody />').append(data)
                            : false;
                    },
                },
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: "&nbsp;",
                    next: "&nbsp;",
                },
            },
            initComplete: function () {
                // Adding role filter once table initialized
                this.api()
                    .columns(3)
                    .every(function () {
                    });
            },
        });
    }
    var item_id;

    // Edit Record
    $(document).on("click", ".edit-record", function (e) {
        e.preventDefault();
        if (
            $(this).attr("data-item") !== "undefined" &&
            $(this).attr("data-item") !== ""
        ) {
            item_id = $(this).attr("data-item");
            $("#editPermissionModal").modal("show");
        }
    });
    $("#update_data").click(function () {
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            type: "put",
            url: "/admin/permission/" + item_id,
            data: {
                id : item_id,
                name: $("#editPermissionName").val(),
            },
        });
        $.ajax({
            beforeSend: function () {
                $("#update_data").text("Updating...");
            },
            success: function( data ) {
                setTimeout(function () {
                    $("#editPermissionModal").modal("hide");
                    $("#update_data").text("Update");
                    $(".datatables-permissions").DataTable().ajax.reload();
                }, 1000);
            }
        });
    });
    // Delete Record
    $(document).on("click", ".delete-record", function (e) {
        e.preventDefault();
        if (
            $(this).attr("data-item") !== "undefined" &&
            $(this).attr("data-item") !== ""
        ) {
            item_id = $(this).attr("data-item");
            $("#deleteModal").modal("show");
        }
    });

    $("#ok_button").click(function () {
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            type: "DELETE",
            url: "/admin/permission/" + item_id,
        });
        $.ajax({
            beforeSend: function () {
                $("#ok_button").text("Deleting...");
            },
            success: function (data) {
                setTimeout(function () {
                    $("#deleteModal").modal("hide");
                    $("#ok_button").text("OK");
                    $(".datatables-permissions").DataTable().ajax.reload();
                }, 1000);
            },
        });
    });

    // Filter form control to default size
    // ? setTimeout used for multilingual table initialization
    setTimeout(() => {
        $(".dataTables_filter .form-control").removeClass("form-control-sm");
        $(".dataTables_length .form-select").removeClass("form-select-sm");
    }, 300);
});
