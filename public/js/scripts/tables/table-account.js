/**
 * DataTables Basic
 */

// const { get } = require("jquery");

$(function () {
    "use strict";

    var dt_basic_table = $(".datatables-basic"),
        dt_date_table = $(".dt-date"),
        assetPath = "../../../app-assets/";

    if ($("body").attr("data-framework") === "laravel") {
        assetPath = $("body").attr("data-asset-path");
    }

    // DataTable with buttons
    // --------------------------------------------------------------------

    if (dt_basic_table.length) {
        var dt_basic = dt_basic_table.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "/admin/datatables/data-account/all",
                data: function (d) {
                    d.klien = $("#klien").val();
                    d.start_date = $("#start_date").val();
                    d.end_date = $("#end_date").val();
                },
                type: "GET",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    ),
                    "Content-Type": "application/json",
                    Accept: "application/json",
                },
                contentType: "application/json",
                dataType: "json",
            },
            columns: [
                { data: "nik" },
                { data: "name" },
                { data: "phone" },
                // { data: "email" },
                { data: "user.name" },
                { data: "bank.name" },
                { data: "action" },
            ],
            columnDefs: [
                {
                    // For Responsive
                    width: "15%",
                    //   responsivePriority: 2,
                    targets: 1,
                },
                {
                    // For Responsive
                    width: "10%",
                    //   responsivePriority: 2,
                    targets: 2,
                },
                {
                    // For Responsive
                    width: "10%",
                    //   responsivePriority: 2,
                    targets: 3,
                },
                {
                    // For Responsive
                    width: "15%",
                    //   responsivePriority: 2,
                    targets: 4,
                },
                {
                    // Actions
                    targets: -1,
                    title: "Actions",
                    orderable: false,
                    width: "10%",
                    render: function (data, type, full, meta) {
                        var $id = full["id"];

                        return (
                            //   $action +
                            '<a href="javascript:;" class="item-edit btn-detail p-25" data-item="' +
                            $id +
                            '">' +
                            feather.icons["eye"].toSvg({
                                class: "font-small-4",
                            }) +
                            "</a>"
                        );
                    },
                },
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50],
            buttons: [
                {
                    extend: "collection",
                    className: "btn btn-outline-secondary dropdown-toggle me-2",
                    text:
                        feather.icons["share"].toSvg({
                            class: "font-small-4 me-50",
                        }) + "Export",
                    buttons: [
                        {
                            extend: "excel",
                            text:
                                feather.icons["file"].toSvg({
                                    class: "font-small-4 me-50",
                                }) + "Excel",
                            className: "dropdown-item",
                            exportOptions: { columns: [0, 1, 2, 3, 4, 5, 6] },
                        },
                    ],
                    init: function (api, node, config) {
                        $(node).removeClass("btn-secondary");
                        $(node).parent().removeClass("btn-group");
                        setTimeout(function () {
                            $(node)
                                .closest(".dt-buttons")
                                .removeClass("btn-group")
                                .addClass("d-inline-flex");
                        }, 50);
                    },
                },
                // {
                //     text:
                //         feather.icons["plus"].toSvg({
                //             class: "me-50 font-small-4",
                //         }) + "Add New Record",
                //     className: "create-new btn btn-primary",
                //     attr: {
                //         "data-bs-toggle": "modal",
                //         "data-bs-target": "#modals-slide-in",
                //     },
                //     init: function (api, node, config) {
                //         $(node).removeClass("btn-secondary");
                //     },
                // },
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return "Details of " + data["full_name"];
                        },
                    }),
                    type: "column",
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                      col.rowIdx +
                                      '" data-dt-column="' +
                                      col.columnIndex +
                                      '">' +
                                      "<td>" +
                                      col.title +
                                      ":" +
                                      "</td> " +
                                      "<td>" +
                                      col.data +
                                      "</td>" +
                                      "</tr>"
                                : "";
                        }).join("");

                        return data
                            ? $('<table class="table"/>').append(
                                  "<tbody>" + data + "</tbody>"
                              )
                            : false;
                    },
                },
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: "&nbsp;",
                    next: "&nbsp;",
                },
            },
        });
        $("#filter").on("submit", function (e) {
            dt_basic.draw();
            e.preventDefault();
            dt_basic.ajax.reload();
            // perhatiin yang di draw harus sesuai dengan deklarasi variabel yang didatatable di atas, terus ngedeklarasiin variabel nya pake let aja
        });
        $("div.head-label").html('<h6 class="mb-0">Pembukaan Rekening</h6>');
    }

    $(document).on("click", ".btn-detail", function (e) {
        e.preventDefault();
        if (
            $(this).attr("data-item") !== "undefined" &&
            $(this).attr("data-item") !== ""
        ) {
            location.href = "/admin/account/detail/" + $(this).attr("data-item");
        }
    });

});
