/**
 * DataTables Basic f
 */

// const { get } = require("jquery");

$(function () {
    'use strict';

    var dt_basic_table = $('.datatables-basic'),
      dt_date_table = $('.dt-date'),
      assetPath = '../../../app-assets/';

    if ($('body').attr('data-framework') === 'laravel') {
      assetPath = $('body').attr('data-asset-path');
    }

    // DataTable with buttons
    // --------------------------------------------------------------------
    var oldExportAction = function (self, e, dt, button, config) {
        if (button[0].className.indexOf('buttons-excel') >= 0) {
            if ($.fn.dataTable.ext.buttons.excelHtml5.available(dt, config)) {
                $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config);
            }
            else {
                $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
            }
        } else if (button[0].className.indexOf('buttons-print') >= 0) {
            $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
        }
    };
    var newExportAction = function (e, dt, button, config) {
        var self = this;
        var oldStart = dt.settings()[0]._iDisplayStart;

        dt.one('preXhr', function (e, s, data) {
            // Just this once, load all data from the server...
            data.start = 0;
            data.length = 2147483647;

            dt.one('preDraw', function (e, settings) {
                // Call the original action function
                oldExportAction(self, e, dt, button, config);

                dt.one('preXhr', function (e, s, data) {
                    // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                    // Set the property to what it was before exporting.
                    settings._iDisplayStart = oldStart;
                    data.start = oldStart;
                });

                // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                setTimeout(dt.ajax.reload, 0);

                // Prevent rendering of the full data to the DOM
                return false;
            });
        });

        // Requery the server with the new one-time export settings
        dt.ajax.reload();
    };

    if (dt_basic_table.length) {
      var dt_basic = dt_basic_table.DataTable({
          processing :true,
          serverSide :true,
          ajax: {
              url: '/admin/datatables/report/all',
              data: function(d) {
                  d.user = $("#user").val();
                  d.downline = $("#downline").val();
                  d.start_date = $("#start_date").val();
                  d.end_date = $("#end_date").val();
              },
              type: "GET",
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                  'Content-Type': 'application/json',
                  Accept: 'application/json'
                },
                contentType: 'application/json',
                dataType: 'json'
          },
        columns: [
          { data: 'upline', visible: false },
          { data: 'username' },
          { data: 'proven_at' },
          { data: 'name' },
          { data: 'phone' },
          { data: 'image' },
          { data: 'status' },
          { data: 'description' },
          { data: 'status', visible: false },
          { data: 'action' },
        ],
        columnDefs: [
            {
            orderable: false,
            targets: 0,
                render: function (data, type, full, meta) {
                    var $upline1 = full['upline1'];
                    var $upline2 = full['upline2'];
                    if ($upline1 != null && $upline2 != null) {
                        return $upline1+' - '+$upline2;
                    } else if ($upline1 == null && $upline2 != null) {
                        return $upline2;
                    } else if ($upline1 == null && $upline2 == null) {
                        return '';
                    }

                }
            },
          {
            // For Responsive
            width: '15%',
            //   responsivePriority: 2,
            targets: 1
          },
          {
            // For Responsive
            width: '10%',
            //   responsivePriority: 2,
            targets: 2
          },
          {
            // For Responsive
            width: '10%',
            //   responsivePriority: 2,
            targets: 3
          },
          {
            // For Responsive
            width: '10%',
            //   responsivePriority: 2,
            targets: 4
          },
          {
            // For Responsive
            width: '10%',
            //   responsivePriority: 2,
            targets: 5,
            render: function (data, type, full, meta) {
                var $id = full['id'];

                var $action = '<d class="d-inline-flex">';

                return (
                //   $action +
                  '<a href="javascript:;" class="btn btn-xl btn-flat-info btn-file p-25" data-item="' + $id + '">' +
                  feather.icons['file'].toSvg({ class: 'font-small-4' }) +
                  '</a>'
                );

              }
          },
          {
            // For Responsive
            width: '10%',
            //   responsivePriority: 2,
            targets: 6,
            render: function (data, type, full, meta) {
                var $id = full['id'];

                switch (full['status']) {
                    case 0:
                        var $color = 'secondary';
                        var $status = 'Pending';
                        break;

                    case 1:
                        var $color = 'success';
                        var $status = 'Valid';
                        break;

                    case 2:
                        var $color = 'warning';
                        var $status = 'Uncompleted';
                        break;

                    case 3:
                        var $color = 'info';
                        var $status = 'Need Follow Up';
                        break;
                }
                return (
                //   $action +
                  '<p class="badge badge-light-'+ $color +'" data-item="' + $id + '">' +
                  $status +
                  '</p>'
                );

              }
          },
          {
            // For Responsive
            width: '10%',
            //   responsivePriority: 2,
            targets: 7,
            render: function (data, type, full, meta) {
                var $id = full['id'];

                switch (full['status']) {
                    case 1:
                        var $color = 'success';
                        return (
                        //   $action +
                          '<p class="badge badge-light-'+ $color +'" data-item="' + $id + '">' +
                          full['description'] +
                          '</p>'
                        );
                        break;

                    case 2:
                        var $color = 'warning';
                        return (
                        //   $action +
                          '<p class="badge badge-light-'+ $color +'" data-item="' + $id + '">' +
                          full['description'] +
                          '</p>'
                        );
                        break;
                    default:
                        return '';
                        break;
                }

              }
          },
          {
            // For Responsive
            width: '10%',
            //   responsivePriority: 2,
            targets: 8,
            render: function (data, type, full, meta) {
                var $id = full['id'];

                switch (full['status']) {
                    case 0:
                        var $color = 'secondary';
                        var $status = 'Pending';
                        break;

                    case 1:
                        var $color = 'success';
                        var $status = 'Valid';
                        break;

                    case 2:
                        var $color = 'warning';
                        var $status = 'Uncompleted - '+full['description'];
                        break;

                    case 3:
                        var $color = 'info';
                        var $status = 'Need Follow Up';
                        break;
                }
                return (
                //   $action +
                  '<p class="badge badge-light-'+ $color +'" data-item="' + $id + '">' +
                  $status +
                  '</p>'
                );

              }
          },
          {
            // Actions
            targets: -1,
            title: 'Actions',
            orderable: false,
            width: '10%',
            render: function (data, type, full, meta) {
                var $id = full['id'];

                switch (full['status']) {
                    case 0:
                        return '';
                        break;

                    case 1:
                        return '';
                        break;

                    default:
                        return (
                            //   $action +
                            // '<a href="javascript:;" class="btn btn-xl btn-flat-info btn-file p-25" data-item="' + $id + '">' +
                            // feather.icons['file'].toSvg({ class: 'font-small-4' }) +
                            // '</a>'
                           ' <div class="btn-group">'+
                            '<button type="button" class="btn btn-sm btn-outline-primary dropdown-toggle dropdown-toggle-split waves-effect waves-float waves-light" data-bs-toggle="dropdown" aria-expanded="false">'+
                                '<span class="visually-hidden">Toggle Dropdown</span>'+
                            '</button>'+
                            '<div class="dropdown-menu dropdown-menu-end">'+
                                '<a class="dropdown-item btn-valid" href="javascript:;" data-item="' + $id + '">Valid</a>'+
                            '</div>'+
                            '</div>'
                            );
                        break;
                }



              }
          }
        ],
        dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        displayLength: 10,
        select: true,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        buttons: [
          {
            extend: 'collection',
            className: 'btn btn-outline-secondary dropdown-toggle me-2',
            text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
            buttons: [
              {
                extend: 'excel',
                title: '',
                text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                className: 'dropdown-item',
                exportOptions: {
                    columns: [0,1,2,3,4,8,7],
                },
                action:newExportAction
              }
            ],
            init: function (api, node, config) {
              $(node).removeClass('btn-secondary');
              $(node).parent().removeClass('btn-group');
              setTimeout(function () {
                $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
              }, 50);
            }
          },
        ],
        responsive: {
          details: {
            display: $.fn.dataTable.Responsive.display.modal({
              header: function (row) {
                var data = row.data();
                return 'Details of ' + data['full_name'];
              }
            }),
            type: 'column',
            renderer: function (api, rowIdx, columns) {
              var data = $.map(columns, function (col, i) {
                return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                  ? '<tr data-dt-row="' +
                      col.rowIdx +
                      '" data-dt-column="' +
                      col.columnIndex +
                      '">' +
                      '<td>' +
                      col.title +
                      ':' +
                      '</td> ' +
                      '<td>' +
                      col.data +
                      '</td>' +
                      '</tr>'
                  : '';
              }).join('');

              return data ? $('<table class="table"/>').append('<tbody>' + data + '</tbody>') : false;
            }
          }
        },
        language: {
          paginate: {
            // remove previous & next text from pagination
            previous: '&nbsp;',
            next: '&nbsp;'
          }
        }
      });
      $('#filter').on('submit', function(e) {
        dt_basic.draw();
        e.preventDefault();
        dt_basic.ajax.reload();
        // perhatiin yang di draw harus sesuai dengan deklarasi variabel yang didatatable di atas, terus ngedeklarasiin variabel nya pake let aja
    });
      $('div.head-label').html('<h6 class="mb-0">Report Agen</h6>');
    }

    $(document).on('click', '.btn-file', function (e) {
        e.preventDefault();
        if ($(this).attr('data-item') !== 'undefined' && $(this).attr('data-item') !== ''){
            window.open(
                '/admin/report/file/' + $(this).attr('data-item'),
                '_blank' // <- This is what makes it open in a new window.
              );
            // location.href = '/admin/report/file/' + $(this).attr('data-item');
        }
      });
      var item_id;
      // Valid action
    $(document).on("click", ".btn-valid", function (e) {
        e.preventDefault();
        if (
            $(this).attr("data-item") !== "undefined" &&
            $(this).attr("data-item") !== ""
        ) {
            item_id = $(this).attr("data-item");
            $("#validModal").modal("show");
        }
    });

    function reset() {
        document.getElementById("filter").reset();
    }


    $("#ok_button").click(function () {
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            type: "POST",
            url: "/admin/report/" + item_id +"/update",
        });
        console.log('a');
        $.ajax({
            beforeSend: function () {
                $("#ok_button").text("Updating Status...");
            },
            success: function (data) {
                setTimeout(function () {
                    $("#validModal").modal("hide");
                    $("#ok_button").text("OK");
                    $(".datatables-basic").DataTable().ajax.reload();
                }, 1000);
            },
        });
    });



  });
