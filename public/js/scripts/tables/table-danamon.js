/**
 * DataTables Basic
 */

// const { get } = require("jquery");

$(function () {
    "use strict";

    var dt_basic_table = $(".datatables-basic"),
        dt_date_table = $(".dt-date"),
        assetPath = "../../../app-assets/";

    if ($("body").attr("data-framework") === "laravel") {
        assetPath = $("body").attr("data-asset-path");
    }

    // DataTable with buttons
    // --------------------------------------------------------------------

    if (dt_basic_table.length) {
        var dt_basic = dt_basic_table.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "/admin/datatables/danamon/all",
                type: "GET",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    ),
                    "Content-Type": "application/json",
                    Accept: "application/json",
                },
                contentType: "application/json",
                dataType: "json",
            },
            columns: [
                { data: "user_mobile" },
                { data: "user_id" },
                { data: "user_full_name" },
                { data: "REF_CODE" },
                { data: "created_time" },
                { data: "user_kyc_status" },
                { data: "signup_env" },
                { data: "NAM_PRODUCT" },
                { data: "has_sync" },
            ],
            columnDefs: [
                {
                    targets: -1,
                    render: function (data, type, full, meta) {
                        var $id = full['id'];

                        switch (full['has_sync']) {
                            case 0:
                                var $color = 'warning';
                                var $status = 'Belum';
                                break;

                            case 1:
                                var $color = 'success';
                                var $status = 'Sudah';
                                break;
                        }
                        return (
                        //   $action +
                          '<p class="badge badge-light-'+ $color +'" data-item="' + $id + '">' +
                          $status +
                          '</p>'
                        );

                      }
                },
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50],
            buttons: [
                {
                    extend: "collection",
                    className: "btn btn-outline-secondary dropdown-toggle me-2",
                    text:
                        feather.icons["share"].toSvg({
                            class: "font-small-4 me-50",
                        }) + "Export",
                    buttons: [
                        {
                            extend: "excel",
                            text:
                                feather.icons["file"].toSvg({
                                    class: "font-small-4 me-50",
                                }) + "Excel",
                            className: "dropdown-item",
                            // exportOptions: { columns: [0, 1, 2, 3, 4, 5, 6] },
                        },
                    ],
                    init: function (api, node, config) {
                        $(node).removeClass("btn-secondary");
                        $(node).parent().removeClass("btn-group");
                        setTimeout(function () {
                            $(node)
                                .closest(".dt-buttons")
                                .removeClass("btn-group")
                                .addClass("d-inline-flex");
                        }, 50);
                    },
                },
                {
                    text:
                        feather.icons["file"].toSvg({
                            class: "me-50 font-small-4",
                        }) + "Input File",
                    className: "create-new btn btn-primary",
                    attr: {
                        "data-bs-toggle": "modal",
                        "data-bs-target": "#inputModal",
                    },
                    init: function (api, node, config) {
                        $(node).removeClass("btn-secondary");
                    },
                },
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return "Details of " + data["full_name"];
                        },
                    }),
                    type: "column",
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.title !== "" // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                      col.rowIdx +
                                      '" data-dt-column="' +
                                      col.columnIndex +
                                      '">' +
                                      "<td>" +
                                      col.title +
                                      ":" +
                                      "</td> " +
                                      "<td>" +
                                      col.data +
                                      "</td>" +
                                      "</tr>"
                                : "";
                        }).join("");

                        return data
                            ? $('<table class="table"/>').append(
                                  "<tbody>" + data + "</tbody>"
                              )
                            : false;
                    },
                },
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: "&nbsp;",
                    next: "&nbsp;",
                },
            },
        });
        $("div.head-label").html('<h6 class="mb-0">Bank</h6>');
    }

    $(document).on("click", ".btn-detail", function (e) {
        e.preventDefault();
        if (
            $(this).attr("data-item") !== "undefined" &&
            $(this).attr("data-item") !== ""
        ) {
            location.href =
                "/admin/bank/" + $(this).attr("data-item") + "/edit";
        }
    });

});
