/**
 * DataTables Basic
 */

// const { get } = require("jquery");

$(function () {
    'use strict';

    var dt_basic_table = $('.datatables-basic'),
      dt_date_table = $('.dt-date'),
      assetPath = '../../../app-assets/';

    if ($('body').attr('data-framework') === 'laravel') {
      assetPath = $('body').attr('data-asset-path');
    }

    // DataTable with buttons
    // --------------------------------------------------------------------

    if (dt_basic_table.length) {
      var dt_basic = dt_basic_table.DataTable({
          processing :true,
          serverSide :true,
          ajax: {
              url: '/admin/datatables/gender/all',
              type: "GET",
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                  'Content-Type': 'application/json',
                  Accept: 'application/json'
                },
                contentType: 'application/json',
                dataType: 'json'
          },
        columns: [
          { data: 'name' },
          { data: 'status' },
          { data: 'action' },
        ],
        columnDefs: [
          {
            // For Responsive
            width: '70%',
            //   responsivePriority: 2,
            targets: 0
          },
          {
            // Label
            width: '15%',
            targets: 1,
            render: function (data, type, full, meta) {
                // console.log(full)
              var $status_number = full['status'];
              var $status = {
                1: { title: 'Active', class: 'badge-light-success' },
                2: { title: 'Non Active', class: ' badge-light-secondary' },
              };
              if (typeof $status[$status_number] === 'undefined') {
                return data;
              }
              return (
                '<span class="badge rounded-pill ' +
                $status[$status_number].class +
                '">' +
                $status[$status_number].title +
                '</span>'
              );
            }
          },
          {
            // Actions
            targets: 2,
            title: 'Actions',
            orderable: false,
            width: '15%',
            render: function (data, type, full, meta) {
                var $id = full['id'];

                var $action = '<d class="d-inline-flex">';

                return (
                //   $action +
                  '<a href="javascript:;" class="item-edit btn-detail p-25" data-item="' + $id + '">' +
                  feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
                  '</a>'+
                  '<a href="javascript:;" class="item-edit btn-delete p-25" data-item="' + $id + '">' +
                  feather.icons['trash'].toSvg({ class: 'font-small-4' }) +
                  '</a>'
                );

              }
          }
        ],
        dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        displayLength: 10,
        lengthMenu: [10, 25, 50],
        buttons: [
          {
            text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New Record',
            className: 'create-new btn btn-primary',
            attr: {
              'data-bs-toggle': 'modal',
              'data-bs-target': '#modals-slide-in'
            },
            init: function (api, node, config) {
              $(node).removeClass('btn-secondary');
            }
          }
        ],
        responsive: {
          details: {
            display: $.fn.dataTable.Responsive.display.modal({
              header: function (row) {
                var data = row.data();
                return 'Details of ' + data['full_name'];
              }
            }),
            type: 'column',
            renderer: function (api, rowIdx, columns) {
              var data = $.map(columns, function (col, i) {
                return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                  ? '<tr data-dt-row="' +
                      col.rowIdx +
                      '" data-dt-column="' +
                      col.columnIndex +
                      '">' +
                      '<td>' +
                      col.title +
                      ':' +
                      '</td> ' +
                      '<td>' +
                      col.data +
                      '</td>' +
                      '</tr>'
                  : '';
              }).join('');

              return data ? $('<table class="table"/>').append('<tbody>' + data + '</tbody>') : false;
            }
          }
        },
        language: {
          paginate: {
            // remove previous & next text from pagination
            previous: '&nbsp;',
            next: '&nbsp;'
          }
        }
      });
      $('div.head-label').html('<h6 class="mb-0">Jenis kelamin</h6>');
    }

    $(document).on('click', '.btn-detail', function (e) {
        e.preventDefault();
        if ($(this).attr('data-item') !== 'undefined' && $(this).attr('data-item') !== ''){
            location.href = '/admin/gender/' + $(this).attr('data-item')+'/edit';
        }
      });

      var claim_id;

      // Delete action
      $(document).on("click", ".btn-delete", function (e) {
          e.preventDefault();
          if (
              $(this).attr("data-item") !== "undefined" &&
              $(this).attr("data-item") !== ""
          ) {
              claim_id = $(this).attr("data-item");
              $("#deleteModal").modal("show");
          }
      });

      $("#ok_button").click(function () {
          $.ajaxSetup({
              headers: {
                  "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
              },
              type: "DELETE",
              url: "/admin/gender/" + claim_id,
          });
          $.ajax({
              beforeSend: function () {
                  $("#ok_button").text("Deleting...");
              },
              success: function (data) {
                  setTimeout(function () {
                      $("#deleteModal").modal("hide");
                      $("#ok_button").text("OK");
                      $(".datatables-basic").DataTable().ajax.reload();
                  }, 1000);
              },
          });
      });
  });
