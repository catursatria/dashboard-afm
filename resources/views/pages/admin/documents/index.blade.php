@extends('layouts/contentLayoutMaster')

@section('title', 'Guidelines & Forms')

@section('page-style')
    {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-knowledge-base.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-faq.css')) }}">
@endsection
@section('content')
    @if (session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <h4 class="alert-heading">Success</h4>
            <div class="alert-body">
                {{ session('success') }}
            </div>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if (session('failed'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <h4 class="alert-heading">Warning</h4>
            <div class="alert-body">
                {{ session('failed') }}
            </div>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <!-- contact us -->
    <section class="faq-contact">
        <div class="row mt-5 pt-75">
            <div class="col-12 text-center">
                <h2>Guidelines</h2>
                <p class="mb-3">
                    {{-- If you cannot find a question in our FAQ, you can always contact us. We will answer to you shortly! --}}
                </p>
            </div>
            <div class="col-sm-4">
                <a href="https://drive.google.com/file/d/1Fxo1f7EISJOHlTkSk8udFCYScoRs9wsQ/view?usp=sharing"
                    target="_blank">
                    <div class="card text-center faq-contact-card shadow-none py-1">
                        <div class="accordion-body">
                            <div class="avatar avatar-tag bg-light-primary mb-2 mx-auto">
                                <i data-feather="file-text" class="font-medium-3"></i>
                            </div>
                            <h4>Tata Tertib</h4>
                            {{-- <span class="text-body">We are always happy to help!</span> --}}
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-4">
                <a href="https://drive.google.com/file/d/16Gk7l5hTrx1jV_dhkfeEJzmCHy1nLYwj/view?usp=sharing"
                    target="_blank">
                    <div class="card text-center faq-contact-card shadow-none py-1">
                        <div class="accordion-body">
                            <div class="avatar avatar-tag bg-light-primary mb-2 mx-auto">
                                <i data-feather="book-open" class="font-medium-3"></i>
                            </div>
                            <h4>Kurikulum</h4>
                            {{-- <span class="text-body">Best way to get answer faster!</span> --}}
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-4">
                <a href="https://drive.google.com/file/d/1BepZCrecs5BnrXqgIWJXc9ZV3vvYo4FR/view?usp=sharing"
                    target="_blank">
                    <div class="card text-center faq-contact-card shadow-none py-1">
                        <div class="accordion-body">
                            <div class="avatar avatar-tag bg-light-primary mb-2 mx-auto">
                                <i data-feather="briefcase" class="font-medium-3"></i>
                            </div>
                            <h4>Sarana Prasarana</h4>
                            {{-- <span class="text-body">Best way to get answer faster!</span> --}}
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>
    <!--/ contact us -->
<br>
    <!-- Knowledge base -->
    <section id="knowledge-base-content">
        <div class="row kb-search-content-info match-height">
            <div class="col-12 text-center">
                <h2>Important Forms</h2>
                <p class="mb-3">
                    {{-- If you cannot find a question in our FAQ, you can always contact us. We will answer to you shortly! --}}
                </p>
            </div>

            <!-- marketing -->
            <div class="col-md-3 col-sm-6 col-12 kb-search-content">
                <div class="card">
                    <a href="https://forms.gle/aThhcgzA8zHwRz3h7" target="_blank">
                        <img src="{{ asset('images/illustration/marketing.svg') }}" class="card-img-top"
                            alt="knowledge-base-image" />
                        <div class="card-body text-center">
                            <h4>Keringanan BOP</h4>
                        </div>
                    </a>
                </div>
            </div>

            <!-- api -->
            <div class="col-md-3 col-sm-6 col-12 kb-search-content">
                <div class="card">
                    <a href="https://forms.gle/dVFV4SQNzwZwQBRC8" target="_blank">
                        <img src="{{ asset('images/illustration/api.svg') }}" class="card-img-top"
                            alt="knowledge-base-image" />
                        <div class="card-body text-center">
                            <h4>Penyerahan Proposal/LPJ</h4>
                        </div>
                    </a>
                </div>
            </div>

            <!-- personalization -->
            <div class="col-md-3 col-sm-6 col-12 kb-search-content">
                <div class="card">
                    <a href="https://forms.gle/snEVRzskXqcbmAx37" target="_blank">
                        <img src="{{ asset('images/illustration/sales.svg') }}" class="card-img-top"
                            alt="knowledge-base-image" />
                        <div class="card-body text-center">
                            <h4>Konsultasi BK</h4>
                        </div>
                    </a>
                </div>
            </div>

            <!-- sales card -->
            <div class="col-md-3 col-sm-6 col-12 kb-search-content">
                <div class="card">
                    <a href="https://forms.gle/v1HyvNtEnEg5SfAa8" target="_blank">
                        <img src="{{ asset('images/illustration/personalization.svg') }}" class="card-img-top"
                            alt="knowledge-base-image" />

                        <div class="card-body text-center">
                            <h4>Pengunduran Diri</h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- Knowledge base ends -->
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/page-knowledge-base.js')) }}"></script>
@endsection
