@extends('layouts/contentLayoutMaster')

@section('title', 'Form Edit')

@section('content')
    <!-- Basic Horizontal form layout section start -->

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Success</h4>
            <div class="alert-body">
                {{ session('success') }}
            </div>
        </div>
    @endif
    @if (session('failed'))
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Warning</h4>
            <div class="alert-body">
                {{ session('failed') }}
            </div>
        </div>
    @endif
    <section id="basic-horizontal-layouts">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Form</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('users.update', $item->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="col-form-label" for="nip">NIP (Nomor Induk Pegawai)</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" id="nip" class="form-control" name="nip"
                                                value="{{ $item->nip }}" />
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="col-form-label" for="nik">NIK (Nomor Induk Kependudukan)</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" id="nik" class="form-control" name="nik"
                                                value="{{ $item->nik }}" />
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="col-form-label" for="name">Name</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" id="name" class="form-control" name="name"
                                                value="{{ $item->name }}" placeholder="Name" />
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="col-form-label" for="email">Email</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="email" id="email" class="form-control" name="email"
                                                value="{{ $item->email }}" />
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="col-form-label" for="password">Password</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="password" id="password" class="form-control" name="password" />
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="col-form-label" for="phone">Phone</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" id="phone" class="form-control" name="phone"
                                                value="{{ $item->phone }}" />
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="col-form-label" for="address">Address</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <textarea name="address" class="form-control" id="address" cols="30"
                                                rows="5">{{ $item->address }}</textarea>
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="col-form-label" for="image">Image</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="file" id="image" class="form-control" name="image" /><br>
                                            @if (!empty($item->image))
                                            <div id="profil">
                                                <img src="{{ asset('storage/Foto Profil/'.$item->image) }}" alt="Foto Profil" width="150" height="auto">
                                            </div>
                                            @else
                                            <p>Foto Belum Ada. Silakan input terlebih dahulu</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="col-form-label" for="name">Upline</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select name="upline_id" class="form-control" id="basic-icon-default-upline">
                                                <option value="">- Pilih Upline -</option>
                                                <option value=""  {{ $item->upline_id == null ? 'selected' : '' }}>Tidak Ada</option>
                                                @foreach ($upline as $value)
                                                    <option value="{{ $value->id }}" {{ $item->upline_id == $value->id ? 'selected' : '' }}>{{ $value->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <div class="col-sm-3">
                                            <label class="col-form-label" for="role">Role</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select name="role" class="form-control" id="role">
                                                <option value="">- Pilih Role -</option>
                                                @foreach ($roles as $value)
                                                    <option value="{{ $value->name }}" {{ $roleUser == $value->name ? 'selected' : '' }}>{{ $value->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-9 offset-sm-3">
                                    <button type="submit" class="btn btn-primary me-1">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

@endsection
