<?php

namespace App\Models\Claim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClaimImage extends Model
{
    use HasFactory;
    protected $table = 'claim_images';

    protected $fillable = [
        'claim_id',
        'identity_card',
        'drive_license',
        'accident_report',
        'witness_letter',
        'payment_receipt',
        'checkup_letter',
        'death_certificate',
        'family_card',
        'support_letter',
        'status',
    ];

    public function claim()
    {
        # code...
        return $this->belongsTo(Claim::class, 'claim_id');
    }
}
