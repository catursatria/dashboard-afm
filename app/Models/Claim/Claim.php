<?php

namespace App\Models\Claim;

use App\Models\Master\Claim as MasterClaim;
use App\Models\Policy\Policy;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{
    use HasFactory;

    protected $fillable = [
        'policy_id',
        'user_id',
        'claim_number',
        'claim_type',
        'bank_name',
        'account_name',
        'account_number',
        'is_vehicle',
        'accident_date',
        'loss',
        'location',
        'chronology',
        'email',
        'status',
        'status_payment',
        'amount',
        'description_of_benefit',
    ];

    public function claim_master()
    {
        # code...
        return $this->belongsTo(MasterClaim::class, 'claim_type');
    }

    public function policy()
    {
        # code...
        return $this->belongsTo(Policy::class, 'policy_id');
    }

    public function user()
    {
        # code...
        return $this->belongsTo(User::class, 'user_id');
    }

    public function image()
    {
        # code...
        return $this->hasOne(ClaimImage::class);
    }
}
