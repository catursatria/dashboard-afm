<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductKnowledge extends Model
{
    use HasFactory;
    protected $connection = 'mysql_master';
    protected $table = 'product_knowledges';

    protected $fillable = [
        "name",
        "file",
        "product_id",
        "status"
    ];

    public function product()
    {
        # code...
        return $this->belongsTo(Product::class, 'product_id');
    }
}
