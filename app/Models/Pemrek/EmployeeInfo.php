<?php

namespace App\Models\Pemrek;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeInfo extends Model
{
    use HasFactory;
    protected $connection = 'mysql_pemrek';
    protected $table = 'employee_info';
    protected $fillable = [
        'customer_id',
        'company_name',
        'profession_code'
    ];

    public function customer()
    {
        # code...
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
