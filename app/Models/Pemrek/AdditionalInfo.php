<?php

namespace App\Models\Pemrek;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdditionalInfo extends Model
{
    use HasFactory;
    protected $connection = 'mysql_pemrek';
    protected $table = 'additional_info';
    protected $fillable = [
        'customer_id',
        'additional_field1',
        'additional_field2',
        'additional_field3',
        'additional_field4',
        'additional_field5',
        'additional_field6',
        'additional_field7',
        'additional_field8',
        'additional_field9',
        'additional_field10'
    ];

    public function customer()
    {
        # code...
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
