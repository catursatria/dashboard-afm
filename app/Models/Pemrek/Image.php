<?php

namespace App\Models\Pemrek;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;
    protected $connection = 'mysql_pemrek';
    protected $fillable = [
        'customer_id',
        'ektp',
        'selfi_ektp',
        'signature',
        'npwp'
    ];

    public function customer()
    {
        # code...
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
