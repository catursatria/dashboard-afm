<?php

namespace App\Models\Pemrek;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonalInfo extends Model
{
    use HasFactory;
    protected $connection = 'mysql_pemrek';
    protected $table = 'personal_info';
    protected $fillable = [
        'customer_id',
        'short_name',
        'home_status',
        'residence_phone_no',
        'id_card_no',
        'npwp',
        'email_id',
        'mobile_no',
    ];

    public function customer()
    {
        # code...
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
