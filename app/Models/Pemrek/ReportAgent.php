<?php

namespace App\Models\Pemrek;

use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use function PHPUnit\Framework\returnSelf;

class ReportAgent extends Model
{
    use HasFactory;
    protected $connection = 'mysql_pemrek';
    protected $table = 'report_agent';
    protected $fillable = [
        'phone',
        'name',
        'image',
        'proven_at',
        'status',
        'description',
        'user_id',
    ];

    public function agen()
    {
        # code...
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getProvenAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

}
