<?php

namespace App\Models\Pemrek;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportDanamon extends Model
{
    use HasFactory;
    protected $connection = 'mysql_pemrek';
    protected $table = 'report_danamon';
    protected $fillable = [
        'REF_CODE',
        'user_id',
        'user_full_name',
        'user_mobile',
        'created_time',
        'user_kyc_status',
        'signup_env',
        'has_sync',
        'NAM_PRODUCT',
    ];

    public $timestamps =  false;
}
