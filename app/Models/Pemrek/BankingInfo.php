<?php

namespace App\Models\Pemrek;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankingInfo extends Model
{
    use HasFactory;
    protected $connection = 'mysql_pemrek';
    protected $table = 'banking_info';
    protected $fillable = [
        'customer_id',
        'product_code',
        'data_transparancy',
        'communication_offering',
        'avg_monthly_income',
        'expected_limit',
        'source_funds',
        'account_purpose',
    ];

    public function customer()
    {
        # code...
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
