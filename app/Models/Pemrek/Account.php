<?php

namespace App\Models\Pemrek;

use App\Models\MasterPemrek\AccountPurpose;
use App\Models\MasterPemrek\Bank;
use App\Models\MasterPemrek\Fund;
use App\Models\MasterPemrek\HomeStatus;
use App\Models\MasterPemrek\Income;
use App\Models\MasterPemrek\Profession;
use App\Models\MasterPemrek\VolumeTransaction;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;
    protected $connection = 'mysql_pemrek';
    protected $table = 'bank_accounts';

    protected $fillable = [
        'name',
        'status_home',
        'residence_phone',
        'nik',
        'npwp',
        'email',
        'phone',

        'address1',
        'address2',
        'address3',
        'postal_code',
        'district_city',
        'province',

        'company_name',
        'profession_code',

        'address1_company',
        'address2_company',
        'address3_company',
        'postal_code_company',
        'district_city_company',
        'province_company',

        'bank_id',
        'avg_monthly_income',
        'expected_limit',
        'source_funds',
        'account_purpose',

        'user_id',
        'image_ektp',
        'image_selfi_ektp',
        'image_signature',
        'image_npwp',
        'image_mobile',
        'has_mobile',
        'status',
    ];

    public function bank()
    {
        # code...
        return $this->belongsTo(Bank::class, 'bank_id');
    }

    public function profession()
    {
        # code...
        return $this->belongsTo(Profession::class, 'profession_code');
    }

    public function user()
    {
        # code...
        return $this->belongsTo(User::class, 'user_id');
    }

    public function monthlyIncome()
    {
        # code...
        return $this->belongsTo(Income::class, 'avg_monthly_income');
    }

    public function limit()
    {
        # code...
        return $this->belongsTo(VolumeTransaction::class, 'expected_limit');
    }

    public function fund()
    {
        # code...
        return $this->belongsTo(Fund::class, 'source_funds');
    }

    public function homeStatus()
    {
        # code...
        return $this->belongsTo(HomeStatus::class, 'status_home');
    }

    public function purpose()
    {
        # code...
        return $this->belongsTo(AccountPurpose::class, 'account_purpose');
    }

    public function getBirthdateAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }
}
