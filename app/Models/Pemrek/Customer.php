<?php

namespace App\Models\Pemrek;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $connection = 'mysql_pemrek';
    protected $fillable = [
        'agen_id',
        'status',
        'reference_no',
        'description',
    ];

    public function personal()
    {
        # code...
        return $this->hasOne(PersonalInfo::class, 'customer_id');
    }

    public function mailling()
    {
        # code...
        return $this->hasOne(MaillingAddress::class, 'customer_id');
    }

    public function spouse()
    {
        # code...
        return $this->hasOne(Spouse::class, 'customer_id');
    }

    public function employee()
    {
        # code...
        return $this->hasOne(EmployeeInfo::class, 'customer_id');
    }

    public function address()
    {
        # code...
        return $this->hasOne(EmployeeAddress::class, 'customer_id');
    }

    public function banking()
    {
        # code...
        return $this->hasOne(BankingInfo::class, 'customer_id');
    }

    public function additional()
    {
        # code...
        return $this->hasOne(AdditionalInfo::class, 'customer_id');
    }

    public function images()
    {
        # code...
        return $this->hasOne(Image::class, 'customer_id');
    }
}
