<?php

namespace App\Models\Pemrek;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeAddress extends Model
{
    use HasFactory;
    protected $connection = 'mysql_pemrek';
    protected $table = 'employee_address';
    protected $fillable = [
        'customer_id',
        'address1',
        'address2',
        'address3',
        'postal_code',
        'district_city',
        'province',
        'country'
    ];

    public function customer()
    {
        # code...
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
