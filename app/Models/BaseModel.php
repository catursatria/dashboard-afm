<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class BaseModel extends Model
{
    protected $connection = "mysql";

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        if (App::environment('testing')) {
            $this->connection = 'mysql';
        }
    }
}
