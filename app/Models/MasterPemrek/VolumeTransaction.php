<?php

namespace App\Models\MasterPemrek;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VolumeTransaction extends Model
{
    use HasFactory;
    protected $connection = 'mysql_pemrek_master';
    protected $table = 'volume_transactions';
    protected $fillable = [
        "name",
    ];
}
