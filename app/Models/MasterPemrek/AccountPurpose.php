<?php

namespace App\Models\MasterPemrek;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountPurpose extends Model
{
    use HasFactory;
    protected $connection = 'mysql_pemrek_master';
    protected $table = 'account_purpose';
    protected $fillable = [
        "name",
    ];
}
