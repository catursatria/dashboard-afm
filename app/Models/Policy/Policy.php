<?php

namespace App\Models\Policy;

use App\Models\Master\Claim;
use App\Models\Master\Job;
use App\Models\Master\Product;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
    use HasFactory;

    protected $fillable = [
        'nik',
        'name',
        'birth_place',
        'birthdate',
        'gender',
        'phone',
        'email',
        'address',
        'village',
        'district',
        'city',
        'province',
        'postal_code',
        'job',
        'product_id',
        'user_id',
        'bank_name',
        'account_name',
        'account_number',
        'image_identity',
        'policy_number',
        'policy_file',
        'e_card',
        'start_date',
        'end_date',
        'status',
    ];

    public function jobs()
    {
        # code...
        return $this->belongsTo(Job::class, 'job');
    }

    public function product()
    {
        # code...
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function user()
    {
        # code...
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getBirthdateAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function getStartDateAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function getEndDateAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }
}
