<?php

namespace App\Exports;

use App\Models\Pemrek\ReportAgent;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;

class ReportAgentExport implements FromCollection
{
    protected $filter;

    function __construct($filter)
    {
        $this->filter = $filter;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $id = Auth::user()->id;
        $datas = ReportAgent::leftJoin('sarasi.users as tb_user', 'report_agent.user_id', '=', 'tb_user.id')
            ->orderBy('created_at', 'desc')
            ->select(['report_agent.*', 'tb_user.upline_id']);

        if (isset($this->filter)) {
            # code...
            switch ($this->filter) {
                case 'ALL':
                    # code...
                    $datas->where('upline_id', $id)
                        ->orWhere('user_id', $id);
                    break;

                case 'SELF':
                    # code...
                    $datas->where('user_id', $id);
                    break;

                default:
                    # code...
                    $datas->where('user_id', $this->filter);
                    break;
            }
        } else {
            # code...
            $datas->where('upline_id', $id)
                ->orWhere('user_id', $id);
        }

        return $datas;
    }
}
