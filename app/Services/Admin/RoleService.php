<?php

namespace App\Services\Admin;

use App\Helpers\Pagination;
use App\Repositories\BaseRepository;
use App\Repositories\RoleRepository;
use App\Services\BaseService;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class RoleService extends BaseService
{
    protected $repo;

    public function __construct(
        BaseRepository $repo
    ) {
        parent::__construct();
        $this->repo = $repo;
    }

    public function store($request)
    {
        try {
            # code...
            // dd($request);
            $role = Role::create(['name' => $request->name]);

            $permissions = $request->permission;
            $role->syncPermissions($permissions);

            return redirect()->route('role.create')->with('success', 'Data has been created.');

        } catch (Exception $exc) {
            # code...
            Log::error($exc);
            return redirect()->back()->with('failed', 'Could not create :object. Please check again.');
        }
    }

    public function data($request)
    {
        $query = Role::query()->get();
        // dd($query);

        return DataTables::of($query)->addIndexColumn()->make(true);
    }

    public function update($request, $id)
    {
        try {
            # code...
            $item = Role::find($id);
            $item->update(['name' => $request->name]);
            $permissions = $request->permission;
            $item->syncPermissions($permissions);

            return redirect()->route('role.index')->with('success', 'Data has been updated.');

        } catch (Exception $exc) {
            # code...
            Log::error($exc);
            return redirect()->back()->with('failed', 'Could not update :object. Please check again.');

        }
    }

    public function destroy($id)
    {
        Role::find($id)->delete();
    }

}
