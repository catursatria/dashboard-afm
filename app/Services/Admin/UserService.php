<?php

namespace App\Services\Admin;

use App\Helpers\Pagination;
use App\Models\User\User;
use App\Repositories\BaseRepository;
use App\Repositories\UserRepository;
use App\Services\BaseService;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class UserService extends BaseService
{
    protected $repo;

    public function __construct(
        UserRepository $repo
    ) {
        parent::__construct();
        $this->repo = $repo;
    }

    public function store($request)
    {
        try {
            # code...
            $data = $request->all();
            $data['password'] = bcrypt($request->password);
            if (!empty($request->image)) {
                # code...
                $nama_file = date('Ymd') . "_" . $data['image']->getClientOriginalName();
                $path = Storage::putFileAs('public/Foto Profil', $data['image'], $nama_file);
                $data['image'] = $nama_file;
            }
            $user = User::create($data);
            $user->assignRole($request->role);

            return redirect()->route('users.index')->with('success', 'Data has been created.');
        } catch (Exception $exc) {
            # code...
            Log::error($exc);

            return redirect()->back()->with('failed', 'Could not create :object. Please check again.');
        }
    }

    public function data($request)
    {
        $query = User::whereHas('roles', function ($query) {
            $query->where('name', '!=', 'super-admin');
         })->get();
        // $query = User::query()->get();

        return DataTables::of($query)->addIndexColumn()->make(true);
    }

    public function update($request, $id)
    {
        try {
            # code...
            $data = $request->all();
            $item = User::find($id);
            if (!empty($request->password)) {
                $data['password'] = bcrypt($request->password);
            } else {
                # code...
                $data['password'] = $item->password;
            }
            if (!empty($request->image)) {
                # code...
                $nama_file = date('Ymd') . "_" . $data['image']->getClientOriginalName();
                $path = Storage::putFileAs('public/Foto Profil', $data['image'], $nama_file);
                $data['image'] = $nama_file;
            }
            $item->update($data);
            $item->syncRoles($request->role);

            return redirect()->route('users.index')->with('success', 'Data has been updated.');

        } catch (Exception $exc) {
            # code...
            Log::error($exc);
            return redirect()->back()->with('failed', 'Could not update :object. Please check again.');

        }
    }

    public function destroy($id)
    {
        User::find($id)->delete();
    }

}
