<?php

namespace App\Services;

use App\Services\BaseService;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class ImageViewService extends BaseService
{

    public function __construct()
    {
        parent::__construct();
    }

    public function showImage($path)
    {
        $path = storage_path($path);

        if (!File::exists($path)) {
            abort(404);
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }
}
