<?php

namespace App\Services;

use App\Models\User\User;
use App\Services\BaseService;
use Illuminate\Support\Facades\Auth;

class AuthService extends BaseService
{

    public function __construct()
    {
        parent::__construct();
    }

    public function login($request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            $collection = collect($user->toArray());
            if (Auth::user()->upline_id) {
                # code...
                $collection->put('upline_name', User::find(Auth::user()->upline_id)->name);
            } else {
                $collection->put('upline_name', null);
            }
            $data['user'] = $collection->all();
            $data['access_token'] = $user->createToken('nApp')->accessToken;
            $permissions = array();
            $dataPermissions = $user->getPermissionsViaRoles();

            foreach ($dataPermissions as $item) {
                array_push($permissions, $item->name);
            }

            $data['permissions'] = $permissions;
            return $this->responseMessage('Login Success', 200, true, $data);
        } else {
            return response()->json(
                [
                    "message" => "These credentials do not match our records.",
                    "error" => "These credentials do not match our records."
                ],
                400
            );

        }
    }
}
