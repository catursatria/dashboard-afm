<?php

namespace App\Repositories;

use App\Models\User\User;
use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function getDownlineByUserId($request, $id)
    {
        $datas = User::where('upline_id', $id)->orderBy('name', 'asc')->get();

        if (isset($request->id)) {
            $datas = User::where('upline_id', $request->id)->orderBy('name', 'asc')->get();
        }

        return $datas;
    }

    public function downline12($id)
    {
        # code...
        $downline = array();
        $downline1 = User::where('upline_id', $id)->get();
        foreach ($downline1 as $value) {
            # code...
            array_push($downline, $value->id);
            $downline2 = User::where('upline_id', $value->id)->get();
            foreach ($downline2 as $item) {
                # code...
                array_push($downline, $item->id);
            }
        }
        return $downline;
    }
}
