<?php

namespace App\Imports;

use App\Models\Policy\Policy;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PolicyImport implements ToModel, WithHeadingRow
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $now = new DateTime();

        return new Policy([
            //
            'nik' => $row['nik'],
            'name' => $row['name'],
            'birth_place' => $row['birth_place'],
            'birthdate' => Carbon::parse($row['birthdate'])->format('Y-m-d'),
            'gender' => $row['gender'],
            'phone' => $row['phone'],
            'email' => $row['email'],
            'address' => $row['address'],
            'village' => $row['village'],
            'district' => $row['district'],
            'city' => $row['city'],
            'province' => $row['province'],
            'postal_code' => $row['postal_code'],
            'product_id' => $row['product_id'],
            'job' => $row['job'],
            'bank_name' => $row['bank_name'],
            'account_name' => $row['account_name'],
            'account_number' => $row['account_number'],
            'created_at'=>$now,
            'updated_at'=>$now,
            'user_id' => Auth::user()->id,

        ]);
    }
}
