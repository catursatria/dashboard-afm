<?php

namespace App\Imports;

use App\Models\Pemrek\ReportDanamon;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ReportDanamonImport implements ToModel, WithHeadingRow
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ReportDanamon([
            'REF_CODE' => $row['ref_code'],
            'user_id' => $row['user_id'],
            'user_full_name' => $row['user_full_name'],
            'user_mobile' => $row['user_mobile'],
            'created_time' => Carbon::parse($row['created_time'])->format('Y-m-d H:i:s'),
            'user_kyc_status' => $row['user_kyc_status'],
            'signup_env' => $row['signup_env'],
            'NAM_PRODUCT' => $row['nam_product'],
            //
        ]);
    }
}
