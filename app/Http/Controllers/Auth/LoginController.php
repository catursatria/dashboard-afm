<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function login(Request $request)
    {
        $this->validate($request, [
            "email" => 'required|email',
            "password" => "required|string|min:6"
        ]);

        $login = [
            'email' => $request->email,
            'password' => $request->password
        ];

        // $db = DB::connection('mysql2');
        $user = User::where('email', $request->email)->where('status', 1)->first();
        // dd($user);
        if ($user->hasRole('super-admin')) {
            if (Auth::attempt($login)) {
                return redirect()->route('dashboard')->with(['success' => 'Authenticated']);
            }
            return redirect()->back()->with(['failed' => 'Please check Your Email/Password again.']);
        }
        return redirect()->back()->with(['failed' => 'You\'re not authorize to perform this action. ']);
    }

}
