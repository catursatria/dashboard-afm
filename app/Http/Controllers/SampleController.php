<?php

namespace App\Http\Controllers\Inbox;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\SampleService;

class SampleController extends Controller
{
  protected $service;

  public function __construct(
    SampleService $service
  ) {
    $this->service = $service;
  }

  public function index(Request $request)
  {
    return $this->service->all($request);
  }

  public function detail($id)
  {
    return $this->service->detail($request->id);
  }

}
