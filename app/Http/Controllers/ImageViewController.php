<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\ImageViewService;

class ImageViewController extends Controller
{
    protected $service;

    public function __construct(
        ImageViewService $service
    ) {
        $this->service = $service;
    }

    public function user($filename)
    {
        return $this->service->showImage('app/public/profil_photos/' . $filename);
    }

    public function reportPemrek($filename)
    {
        return $this->service->showImage('app/public/dmobile/' . $filename);
    }
}
