<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User\User;
use App\Services\Admin\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    protected $service;

    public function __construct(
        UserService $service

    )
    {
        $this->service = $service;
    }

    public function index()
    {
        # code...
        $upline = User::whereHas('roles', function ($query) {
            $query->where('name', '!=', 'super-admin');
         })->orderBy('name', 'asc')->get();
        $roles = Role::all();
        return view('pages.admin.user.index', compact('upline', 'roles'));
    }

    public function data(Request $request)
    {
        # code...
        return $this->service->data($request);
    }

    public function edit($id)
    {
        # code...
        $item = User::find($id);
        $upline = User::whereHas('roles', function ($query) {
            $query->where('name', '!=', 'super-admin');
         })->where('id', '!=', $item->id)->get();
        $roles = Role::all();
        $roleUser = $item->getRoleNames()[0];
        return view('pages.admin.user.edit', compact('item', 'upline', 'roles', 'roleUser'));
    }

    public function store(Request $request)
    {
        # code...
        return $this->service->store($request);
    }

    public function update(Request $request, $id)
    {
        # code...
        return $this->service->update($request, $id);
    }

    public function destroy($id)
    {
        # code...
        return $this->service->destroy($id);
    }
}
