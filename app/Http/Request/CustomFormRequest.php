<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

class CustomFormRequest extends FormRequest
{
    protected $mainDatabase = "mysql";

    public function __construct()
    {
        parent::__construct();

        if (App::environment('testing')) {
            $this->mainDatabase = 'mysql';
        }
    }
}
