<?php

use App\Http\Controllers\Admin\BankAccountController;
use App\Http\Controllers\Admin\ClaimController as AdminClaimController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\Documents\DocumentController;
use App\Http\Controllers\Admin\Master\BankController;
use App\Http\Controllers\Admin\Master\ClaimController;
use App\Http\Controllers\Admin\Master\GenderController;
use App\Http\Controllers\Admin\Master\JobController;
use App\Http\Controllers\Admin\Master\ProductController;
use App\Http\Controllers\Admin\Master\ProductKnowledgeController;
use App\Http\Controllers\Admin\PolicyController;
use App\Http\Controllers\Admin\ReportController;
use App\Http\Controllers\Admin\ReportDanamonController;
use App\Http\Controllers\Admin\RolePermission\PermissionsController;
use App\Http\Controllers\Admin\RolePermission\RolesController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Claim\ClaimImageController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StaterkitController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\ImageViewController;
use App\Http\Controllers\Pemrek\Account\AccountController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/a', [StaterkitController::class, 'home'])->name('home');
// Route::get('home', [StaterkitController::class, 'home'])->name('home');
// Route Components
Route::get('layouts/collapsed-menu', [StaterkitController::class, 'collapsed_menu'])->name('collapsed-menu');
Route::get('layouts/full', [StaterkitController::class, 'layout_full'])->name('layout-full');
Route::get('layouts/without-menu', [StaterkitController::class, 'without_menu'])->name('without-menu');
Route::get('layouts/empty', [StaterkitController::class, 'layout_empty'])->name('layout-empty');
Route::get('layouts/blank', [StaterkitController::class, 'layout_blank'])->name('layout-blank');

// locale Route
Route::get('lang/{locale}', [LanguageController::class, 'swap']);
Route::get('/', [DashboardController::class, 'admin'])->name('dashboard');
Route::get('/documents', [DocumentController::class, 'index'])->name('document.index');
Route::group(['middleware' => ['auth', 'role:super-admin']], function () {
    //

    Route::prefix('admin')->group(function () {
        Route::resource('role', RolesController::class);
        Route::resource('permission', PermissionsController::class);

        Route::group(['prefix' => 'datatables'], function () {
            Route::get('/permission/all', [PermissionsController::class, 'data'])->name('permission.data');
            Route::get('/role/all', [RolesController::class, 'data'])->name('role.data');
            Route::get('/bank/all', [BankController::class, 'data'])->name('bank.data');
        });
    });
});

Auth::routes();
