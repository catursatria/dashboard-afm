<?php

use App\Http\Controllers\Admin\Master\GenderController;
use App\Http\Controllers\Admin\Master\JobController;
use App\Http\Controllers\Admin\Master\ProductController;
use App\Http\Controllers\Admin\ReportController as AdminReportController;
use App\Http\Controllers\Auth_API\ChangePasswordController;
use App\Http\Controllers\Auth_API\LoginController;
use App\Http\Controllers\Claim\ClaimController;
use App\Http\Controllers\Claim\ClaimImageController;
use App\Http\Controllers\Pemrek\Account\AccountController;
use App\Http\Controllers\Pemrek\DailyReport\DailyReportController;
use App\Http\Controllers\Pemrek\Report\ReportController;
use App\Http\Controllers\Policy\PolicyController;
use App\Http\Controllers\User\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [LoginController::class, 'login']);
Route::get('/dropdown/report/get-downline', [AdminReportController::class, 'getDownline'])->name('get.downline');
Route::group(['middleware' => 'auth:api'], function () {
    // Route::resource('policy', PolicyController::class);
    Route::get('claim-image', [ClaimImageController::class, 'index'])->name('claim-image.index');


    Route::group(['prefix' => 'account'], function () {
        Route::get('/', [AccountController::class, 'index'])->name('account.index');
        Route::get('/{id}', [AccountController::class, 'show'])->name('account.show');
        Route::post('/personal', [AccountController::class, 'personal'])->name('account.personal');
        Route::post('/mailling/{id}', [AccountController::class, 'mailling'])->name('account.mailling');
        Route::post('/spouse/{id}', [AccountController::class, 'spouse'])->name('account.spouse');
        Route::post('/employee/{id}', [AccountController::class, 'employee'])->name('account.employee');
        Route::post('/employee-address/{id}', [AccountController::class, 'employeeAddress'])->name('account.employee.address');
        Route::post('/banking/{id}', [AccountController::class, 'banking'])->name('account.banking');
        Route::post('/additional/{id}', [AccountController::class, 'additional'])->name('account.additional');
        Route::post('/images/{id}', [AccountController::class, 'images'])->name('account.images');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', [UserController::class, 'show'])->name('user.show');
        Route::post('/password', [ChangePasswordController::class, 'change'])->name('change.password');
        Route::post('/update', [UserController::class, 'update'])->name('user.update');
        Route::post('/image', [UserController::class, 'imageStore'])->name('user.image-store');
        Route::get('/statistic', [UserController::class, 'statistic'])->name('user.statistic');
        Route::get('/downline', [UserController::class, 'downline'])->name('user.downline');
    });

    Route::group(['prefix' => 'master'], function () {
        Route::get('/job', [JobController::class, 'master'])->name('job.master');
        Route::get('/gender', [GenderController::class, 'master'])->name('gender.master');
        Route::get('/product', [ProductController::class, 'master'])->name('product.master');
    });

    Route::group(['prefix' => 'policy'], function () {
        Route::post('/', [PolicyController::class, 'store'])->name('policy.store');
        Route::get('/', [PolicyController::class, 'index'])->name('policy.index');
        Route::post('/{id}', [PolicyController::class, 'update'])->name('policy.update');
        Route::post('/import-excel', [PolicyController::class, 'importExcel'])->name('import.excel');
        Route::post('/order/{id}', [PolicyController::class, 'order'])->name('policy.order');
        Route::post('/payment/{id}', [PolicyController::class, 'payment'])->name('policy.payment');
        Route::post('/info/{id}', [PolicyController::class, 'info'])->name('policy.info');
    });

    Route::group(['prefix' => 'claim'], function () {
        Route::get('/', [ClaimController::class, 'index'])->name('claim.index');
        Route::post('/', [ClaimController::class, 'store'])->name('claim.store');
        Route::get('/{id}', [ClaimController::class, 'show'])->name('claim.show');
        Route::post('/update', [ClaimController::class, 'update'])->name('claim.update');
        Route::post('/upload/{id}', [ClaimImageController::class, 'upload'])->name('claim-image.upload');
        Route::post('/update-status/{id}', [ClaimController::class, 'updateStatus'])->name('claim.update.status');
        Route::post('/status-payment/{id}', [ClaimController::class, 'statusPayment'])->name('claim.status.payment');
    });

    Route::group(['prefix' => 'report'], function () {
        Route::get('/', [ReportController::class, 'index'])->name('report.index');
        Route::get('/excel/export', [ReportController::class, 'export'])->name('report.export');
        Route::get('/list', [ReportController::class, 'dataDropdown'])->name('report.list');
        Route::post('/add', [ReportController::class, 'store'])->name('report.store');
    });
});

Route::group(['prefix' => 'daily-report'], function () {
    Route::get('/pemrek', [DailyReportController::class, 'pemrek'])->name('daily-report.index');
});
