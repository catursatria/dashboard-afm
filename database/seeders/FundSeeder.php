<?php

namespace Database\Seeders;

use App\Models\MasterPemrek\Fund;
use Illuminate\Database\Seeder;

class FundSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Fund::create([
            'id' => '01',
            'name' => 'Pribadi',
        ]);
        Fund::create([
            'id' => '02',
            'name' => 'Pribadi dan Usaha',
        ]);
        Fund::create([
            'id' => '04',
            'name' => 'Usaha / Hasil Usaha',
        ]);
        Fund::create([
            'id' => '05',
            'name' => 'Lainnya',
        ]);
        Fund::create([
            'id' => '07',
            'name' => 'Dana Pinjaman',
        ]);
        Fund::create([
            'id' => '08',
            'name' => 'Gaji',
        ]);
        Fund::create([
            'id' => '09',
            'name' => 'Insentif',
        ]);
        Fund::create([
            'id' => '10',
            'name' => 'Dana Pihak Lain',
        ]);
    }
}
