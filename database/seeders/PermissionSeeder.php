<?php

namespace Database\Seeders;

use App\Models\User\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        // app()[PermissionRegistrar::class]->forgetCachedPermissions();

         // create permissions
         Permission::create(['name' => 'policy-view']);
         Permission::create(['name' => 'policy-create']);
         Permission::create(['name' => 'claim-view']);
         Permission::create(['name' => 'claim-create']);
         Permission::create(['name' => 'pemrek-view']);
         Permission::create(['name' => 'pemrek-create']);

         // create roles and assign existing permissions
        $role1 = Role::create(['name' => 'sales1']);
        $role1->givePermissionTo('policy-view');
        $role1->givePermissionTo('policy-create');
        $role1->givePermissionTo('claim-view');
        $role1->givePermissionTo('claim-create');
        $role2 = Role::create(['name' => 'sales2']);
        $role2->givePermissionTo('policy-view');
        $role2->givePermissionTo('claim-view');
        $role3 = Role::create(['name' => 'pemrek']);
        $role3->givePermissionTo('pemrek-view');
        $role3->givePermissionTo('pemrek-create');

        $role4 = Role::create(['name' => 'super-admin']);
        // gets all permissions via Gate::before rule; see AuthServiceProvider

        // create demo users
        $user = User::create([
            'name' => 'John Doe',
            'email' => 'john@test.com',
            'password' => bcrypt('1234567890'),
            'phone' => '081234567890',
            'address' => 'Gg. Sawo no. 33B',
            'nik' => '33221006512731'
        ]);
        $user->assignRole($role1);

        $user = User::create([
            'name' => 'Jane Doe',
            'email' => 'jane@test.com',
            'password' => bcrypt('1234567890'),
            'phone' => '085321456790',
            'address' => 'Gg. Sawo no. 30B',
            'nik' => '33221006512729'
        ]);
        $user->assignRole($role2);

        $user = User::create([
            'name' => 'Peter Parkour',
            'email' => 'peter@test.com',
            'password' => bcrypt('1234567890'),
            'phone' => '084123456790',
            'address' => 'Gg. Sawo no. 30B',
            'nik' => '33221006512731'
        ]);
        $user->assignRole($role3);

        $user = User::create([
            'name' => 'Super Admin',
            'email' => 'superadmin@test.com',
            'password' => bcrypt('1234567890'),
        ]);
        $user->assignRole($role4);

    }
}
