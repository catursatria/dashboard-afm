<?php

namespace Database\Seeders;

use App\Models\MasterPemrek\Bank;
use Illuminate\Database\Seeder;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Bank::create([
            'id' => 459,
            'name' => 'Bank DANAMON'
        ]);

    }
}
