<?php

namespace Database\Seeders;

use App\Models\MasterPemrek\Profession;
use Illuminate\Database\Seeder;

class ProfessionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Profession::create([
            'id' => 11,
            'name' => 'TNI/ABRI/Militer',
        ]);
        Profession::create([
            'id' => 14,
            'name' => 'Wiraswasta',
        ]);
        Profession::create([
            'id' => 16,
            'name' => 'Pegawai Swasta',
        ]);
        Profession::create([
            'id' => 51,
            'name' => 'Polisi',
        ]);
        Profession::create([
            'id' => 62,
            'name' => 'Pegawai Negeri',
        ]);
        Profession::create([
            'id' => 64,
            'name' => 'Akuntan Publik/Perencanaan Keuangan/Konsultan Pajak/Kurator & Karyawannya',
        ]);
        Profession::create([
            'id' => 68,
            'name' => 'Guru/Dosen',
        ]);
        Profession::create([
            'id' => 75,
            'name' => 'Pegawai BUMN',
        ]);
        Profession::create([
            'id' => 83,
            'name' => 'Professional',
        ]);
        Profession::create([
            'id' => 84,
            'name' => 'Dokter',
        ]);
        Profession::create([
            'id' => 85,
            'name' => 'Arsitek',
        ]);
    }
}
