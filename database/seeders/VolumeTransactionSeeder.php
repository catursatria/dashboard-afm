<?php

namespace Database\Seeders;

use App\Models\MasterPemrek\VolumeTransaction;
use Illuminate\Database\Seeder;

class VolumeTransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        VolumeTransaction::create([
            'id' => 1,
            'name' => '<500 Ribu',
        ]);
        VolumeTransaction::create([
            'id' => 2,
            'name' => '500 Ribu - 1 Juta',
        ]);
        VolumeTransaction::create([
            'id' => 3,
            'name' => '1 - 5 Juta',
        ]);
        VolumeTransaction::create([
            'id' => 4,
            'name' => '5 - 10 Juta',
        ]);
        VolumeTransaction::create([
            'id' => 5,
            'name' => '10 - 25 Juta',
        ]);
        VolumeTransaction::create([
            'id' => 6,
            'name' => '25 - 50 Juta',
        ]);
        VolumeTransaction::create([
            'id' => 7,
            'name' => '10 - 100 Juta',
        ]);
        VolumeTransaction::create([
            'id' => 8,
            'name' => '100 - 500 Juta',
        ]);
        VolumeTransaction::create([
            'id' => 9,
            'name' => '500 Juta - 1 Milyar',
        ]);
        VolumeTransaction::create([
            'id' => 10,
            'name' => '1 - 5 Milyar',
        ]);
        VolumeTransaction::create([
            'id' => 11,
            'name' => '>5 Milyar',
        ]);
    }
}
