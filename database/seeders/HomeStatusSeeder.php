<?php

namespace Database\Seeders;

use App\Models\MasterPemrek\HomeStatus;
use Illuminate\Database\Seeder;

class HomeStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        HomeStatus::create(['name' => 'Milik Sendiri']);
        HomeStatus::create(['name' => 'Orang Tua']);
        HomeStatus::create(['name' => 'Dinas']);
        HomeStatus::create(['name' => 'Kontrakan/sewa']);
        HomeStatus::create([
            'id' => 9,
            'name' => 'lainnya',
        ]);
    }
}
