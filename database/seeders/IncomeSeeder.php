<?php

namespace Database\Seeders;

use App\Models\MasterPemrek\Income;
use Illuminate\Database\Seeder;

class IncomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Income::create([
            'id' => 11,
            'name' => '<500 Ribu',
        ]);
        Income::create([
            'id' => 12,
            'name' => '500 Ribu - 1 Juta',
        ]);
        Income::create([
            'id' => 13,
            'name' => '1 - 5 Juta',
        ]);
        Income::create([
            'id' => 14,
            'name' => '5 - 10 Juta',
        ]);
        Income::create([
            'id' => 15,
            'name' => '10 - 25 Juta',
        ]);
        Income::create([
            'id' => 16,
            'name' => '25 - 50 Juta',
        ]);
        Income::create([
            'id' => 17,
            'name' => '10 - 100 Juta',
        ]);
        Income::create([
            'id' => 18,
            'name' => '100 - 500 Juta',
        ]);
        Income::create([
            'id' => 19,
            'name' => '500 Juta - 1 Milyar',
        ]);
        Income::create([
            'id' => 20,
            'name' => '1 - 5 Milyar',
        ]);
        Income::create([
            'id' => 21,
            'name' => '>5 Milyar',
        ]);
    }
}
