<?php

namespace Database\Seeders;

use App\Models\MasterPemrek\AccountPurpose;
use Illuminate\Database\Seeder;

class AccountPurposeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        AccountPurpose::create([
            'id' => '01',
            'name' => 'Pribadi',
        ]);
        AccountPurpose::create([
            'id' => '02',
            'name' => 'Usaha',
        ]);
        AccountPurpose::create([
            'id' => '03',
            'name' => 'Pribadi dan Usaha',
        ]);
        AccountPurpose::create([
            'id' => '06',
            'name' => 'Fasilitas Pinjaman',
        ]);
        AccountPurpose::create([
            'id' => '07',
            'name' => 'Simpanan',
        ]);
        AccountPurpose::create([
            'id' => '08',
            'name' => 'Gaji',
        ]);
        AccountPurpose::create([
            'id' => '09',
            'name' => 'Investasi',
        ]);
        AccountPurpose::create([
            'id' => '10',
            'name' => 'Pembayaran Fasilitas Pinjaman',
        ]);
        AccountPurpose::create([
            'id' => '11',
            'name' => 'Lain-lain',
        ]);
    }
}
