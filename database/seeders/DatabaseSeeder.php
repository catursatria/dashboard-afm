<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(AccountPurposeSeeder::class);
        $this->call(BankSeeder::class);
        $this->call(FundSeeder::class);
        $this->call(VolumeTransactionSeeder::class);
        $this->call(ProfessionSeeder::class);
        $this->call(IncomeSeeder::class);
        $this->call(HomeStatusSeeder::class);
        $this->call(PermissionSeeder::class);
    }
}
